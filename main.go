package main

import (
	"fake-beers-lab/core/adding"
	"fake-beers-lab/core/beers"
	"fake-beers-lab/infra"
	"fmt"
)

func main() {

	// create server

	// create repos
	beersRepository := infra.NewRepository()

	// map handlers to server
	beersAddingHandler := adding.NewHandler(beersRepository)

	beersAddingHandler.BeerAddHandler("baltica 17", "baltica")

	fmt.Println(beers.Beer{Title: "baltica 17", Brand: "baltica"})
}