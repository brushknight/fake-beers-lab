package adding

import "fake-beers-lab/core/beers"

type BeerAddingHandler struct {
	repository beers.Repository
}

func (h *BeerAddingHandler) BeersHandler(title string, brand string) {

	beer := beers.Beer{Title: title, Brand: brand}
	h.repository.Save(beer)
}

func NewHandler(repository beers.Repository) *BeerAddingHandler {
	return &BeerAddingHandler{repository: repository}
}