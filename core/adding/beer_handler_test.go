package adding

import (
	"fake-beers-lab/core/beers"
	"testing"
)

func TestBeerAddingHandler_BeerAddHandler(t *testing.T) {

	repository := beers.NewRepositoryMock()
	handler := NewHandler(repository)

	handler.BeersHandler("seven", "baltica")

	list, err := repository.GetAll()

	if err != nil {
		t.Errorf("Unexpected error occurs: %e.", err)
	}

	if len(list) != 1 {
		t.Errorf("Repository length expected: %d, got: %d.", 1, len(list))
	}

	if len(list) == 1 && list[0].Id != 1 {
		t.Errorf("Saved in repository entity has wrond Id, expected: %d, got: %d.", 1, list[0].Id)
	}
}
