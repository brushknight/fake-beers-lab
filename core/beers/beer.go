package beers

type Beer struct {
	Id int
	Title string
	Brand string
}
