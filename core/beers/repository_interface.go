package beers

type Repository interface {
	GetOneById(id int) (Beer, error)
	GetAll() ([]Beer, error)
	Save(beer Beer) (bool, error)
}
