package beers

import "errors"

type RepositoryMock struct {
	list   []Beer
	lastId int // may be a collision if multi thread will be run here
}

var ErrNotFound = errors.New("beer not found")

func (r *RepositoryMock) GetOneById(id int) (Beer, error) {

	var beer Beer

	for i := range r.list {
		if r.list[i].Id == id {
			beer.Id = r.list[i].Id
			beer.Title = r.list[i].Title
			beer.Brand = r.list[i].Brand

			return beer, nil
		}
	}

	return beer, ErrNotFound
}

func (r *RepositoryMock) GetAll() ([]Beer, error) {

	return r.list, nil
}

func (r *RepositoryMock) Save(beer Beer) (bool, error) {

	beer.Id = r.lastId + 1
	r.lastId++
	r.list = append(r.list, beer)

	return true, nil
}

func NewRepositoryMock() *RepositoryMock {
	return new(RepositoryMock)
}