package infra

import (
	"fake-beers-lab/core/beers"
	"reflect"
	"testing"
)

func TestBeersRepositoryMemory_Save(t *testing.T) {
	repository := NewRepository()

	beerTitle := "seven"
	beerBrand := "baltica"

	beerOne := beers.Beer{Title: beerTitle, Brand: beerBrand}

	repository.Save(beerOne)

	if len(repository.list) != 1 {
		t.Errorf("Repository length is not correct, got: %d, want: %d.", len(repository.list), 1)
	}

	beerFromRepository := repository.list[0]

	if beerFromRepository.Id != 1 {
		t.Errorf("Inserted model Id is incorrect, got: %d, want: %d.", beerFromRepository.Id, 2)
	}

	if beerFromRepository.Title != beerTitle {
		t.Errorf("Inserted model Title is incorrect, got: %s, want: %s.", beerFromRepository.Title, beerTitle)
	}

	if beerFromRepository.Brand != beerBrand {
		t.Errorf("Inserted model Brand is incorrect, got: %s, want: %s.", beerFromRepository.Brand, beerBrand)
	}

	/**** Second scenario ****/

	beerTitleTwo := "can"
	beerBrandTwo := "Heineken"
	beerTwo := beers.Beer{Title: beerTitleTwo, Brand: beerBrandTwo}
	repository.Save(beerTwo)

	if len(repository.list) != 2 {
		t.Errorf("Repository length is not correct, got: %d, want: %d.", len(repository.list), 2)
	}

	beerFromRepositoryTwo := repository.list[1]

	if beerFromRepositoryTwo.Id != 2 {
		t.Errorf("Inserted model Id is incorrect, got: %d, want: %d.", beerFromRepositoryTwo.Id, 2)
	}

	if beerFromRepositoryTwo.Title != beerTitleTwo {
		t.Errorf("Inserted model Title is incorrect, got: %s, want: %s.", beerFromRepositoryTwo.Title, beerTitleTwo)
	}

	if beerFromRepositoryTwo.Brand != beerBrandTwo {
		t.Errorf("Inserted model Brand is incorrect, got: %s, want: %s.", beerFromRepositoryTwo.Brand, beerBrandTwo)
	}
}

func TestBeersRepositoryMemory_GetAll(t *testing.T) {

	var list []beers.Beer

	list = append(list, beers.Beer{1, "seven", "Baltica"})
	list = append(list, beers.Beer{2, "can", "Heineken"})

	repository := BeersRepositoryMemory{list: list}

	resultFromRepository, _ := repository.GetAll()

	countOfElements := len(resultFromRepository)

	if countOfElements != 2 {
		t.Errorf("Repository length is not correct, got: %d, want: %d.", countOfElements, 2)
	}

	if !reflect.DeepEqual(list, resultFromRepository) {
		t.Errorf("Repository response is not as expected, got: %v, want: %v.", resultFromRepository, list)
	}
}

func TestBeersRepositoryMemory_GetOneById(t *testing.T) {

	var list []beers.Beer

	list = append(list, beers.Beer{1, "seven", "Baltica"})
	list = append(list, beers.Beer{2, "can", "Heineken"})

	repository := BeersRepositoryMemory{list: list}

	beer, err := repository.GetOneById(2)

	if err != nil {
		t.Errorf("Error occured: %e", err)
	}

	if beer.Id != 2 {
		t.Errorf("Inserted model Id is incorrect, got: %d, want: %d.", beer.Id, 2)
	}

	/**** Second scenario ****/

	beer, err = repository.GetOneById(3)

	if err != ErrNotFound {
		t.Errorf("Error expected: %e, received: %e", ErrNotFound, err)
	}

	if beer != (beers.Beer{}) {
		t.Errorf("Model expected to be empty, got: %v.", beer)
	}
}
