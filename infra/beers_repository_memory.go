package infra

import (
	"errors"
	"fake-beers-lab/core/beers"
)

type BeersRepositoryMemory struct {
	list   []beers.Beer
	lastId int // may be a collision if multi thread will be run here
}

var ErrNotFound = errors.New("beer not found")

func (r *BeersRepositoryMemory) GetOneById(id int) (beers.Beer, error) {

	var beer beers.Beer

	for i := range r.list {
		if r.list[i].Id == id {
			beer.Id = r.list[i].Id
			beer.Title = r.list[i].Title
			beer.Brand = r.list[i].Brand

			return beer, nil
		}
	}

	return beer, ErrNotFound
}

func (r *BeersRepositoryMemory) GetAll() ([]beers.Beer, error) {

	return r.list, nil
}

func (r *BeersRepositoryMemory) Save(beer beers.Beer) (bool, error) {

	beer.Id = r.lastId + 1
	r.lastId++
	r.list = append(r.list, beer)

	return true, nil
}

func NewRepository() *BeersRepositoryMemory {
	return new(BeersRepositoryMemory)
}
